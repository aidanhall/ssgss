include config.mk

MDDIRS := $(shell find $(MDROOTDIR) -type d)

SITEDIRS := $(subst $(MDROOTDIR), $(SITEROOTDIR), $(MDDIRS))


vpath %.md $(MDDIRS)

MDFILES = $(shell find $(MDROOTDIR) -type f -name "*.md") $(ROLLINGBLOGFILE)
BLOGFILES = $(shell find $(BLOGDIR) -type f -name "*_*.md" | sort -rn)
HTMLFILES := $(subst $(MDROOTDIR), $(SITEROOTDIR), $(patsubst %.md, %.html, $(MDFILES)))


# implicit(?) rules
$(SITEROOTDIR)/%.html : %.md $(HEADERFILE) $(FOOTERFILE)
	mkdir -p $(shell dirname $@)
	$(MARKDOWN) < $<\
		| $(M4)\
		-D PAGETITLE="$(shell awk '/^#/{ sub(/^# /, NULL); print; exit}' $< )" \
		-D DATE="$(shell date +'%d/%m/%y')" \
		-D COMMITDATE="$(shell git log --date=format:"%d/%m/%y" --pretty=format:'%cd' -n 1 $<)" \
		$(M4FILES) $(HEADERFILE) - $(FOOTERFILE)\
		| sed '/^\s*$$/d' \
		> $@


# explicit rules

all: $(ROLLINGBLOGFILE) $(HTMLFILES) verbatim

$(HTMLFILES): $(M4FILES)

$(ROLLINGBLOGFILE): $(ROLLINGBLOGTEMPLATE) $(BLOGFILES)
	echo prerequs: $^
	cat $^ > $@

$(SITEROOTDIR):
	mkdir $@


verbatim: $(RAWDIR)
	cp -ruv $(RAWDIR)/* $(SITEROOTDIR)


clean:
	$(RM) -rv $(SITEROOTDIR)/*

force: clean all

upload: all
	rsync -avXuz --delete-after $(SITEROOTDIR)/\
		$(WEBSERVER)$(SERVERDIR)

view: all
	setsid xdg-open $(SITEROOTDIR)/index.html &
