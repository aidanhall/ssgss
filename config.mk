# progs
M4 = m4 -P
MARKDOWN = markdown

# dirs
ROOT = $(PWD)

TEMPLATEDIR = $(ROOT)/templates

MDROOTDIR = $(ROOT)/markdown

SITEROOTDIR = $(ROOT)/site

RAWDIR = $(ROOT)/raw

BLOGDIR = $(MDROOTDIR)/blog

# server
WEBSERVER = mark@facebook.com:22
SERVERDIR = /var/www/html

# files

M4FILES  = $(ROOT)/util.m4 $(ROOT)/config.m4
HEADERFILE = $(TEMPLATEDIR)/header.html.m4
FOOTERFILE = $(TEMPLATEDIR)/footer.html.m4
ROLLINGBLOGTEMPLATE = $(TEMPLATEDIR)/rollingblog.md

ROLLINGBLOGFILE = $(BLOGDIR)/rolling.md
