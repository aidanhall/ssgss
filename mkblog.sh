#!/bin/sh

BLOGDIR="markdown/blog"
BLOGINDEX="$BLOGDIR/index.md"
# BLOGROLLING="$BLOGDIR/rolling.md"

printf "Blog Title: "
read -r BLOGTITLE
echo blogging "$BLOGTITLE"

mkdir -p "$BLOGDIR"


BLOGNAME="$(date +'%y-%m-%d_%H.%M')"
BLOGDATE="$(date +'%d/%m/%y, %H:%M')"
BLOGFILE="$BLOGNAME.md"

sed -i "/^## Posts/a * [$BLOGTITLE - $BLOGDATE](SSGPAGE($BLOGFILE))" "$BLOGINDEX"
# sed -i "/^## Posts/a <iframe class=rollingblogentry loading=lazy title=\"$BLOGNAME\" src=SSGPAGE($BLOGFILE)></iframe>" "$BLOGROLLING"
sed "s/BLOGTITLE/$BLOGTITLE/g; s/DATE/$(date)/g;" templates/blog.md > "$BLOGDIR/$BLOGFILE"
# echo "# $BLOGTITLE" > "$BLOGDIR/$BLOGFILE"

# "$EDITOR" "$BLOGDIR/$BLOGFILE"
echo "Created blog file: $BLOGDIR/$BLOGFILE"
