<hr>
<footer>
<p>
Copyright AUTHORNAME 2022.
Last <abbr title="Most recent Git commit.">change</abbr>:
COMMITDATE.
<a href="https://gitlab.com/aidanhall/ssgss"
   title='Using "Static Site Converter Super Simple"'>Compiled</a>
on DATE.
<a href="#top">Top 🔝</a>
</p>
</footer>
</body></html>
